package com.nostratech.gojek.util;

//import org.joda.time.format.DateTimeFormat;
//import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Constants value
 * @author andri.khrisharyadi@nostratech.com
 */
public class Constants {

    public static final String MERCHANT_TRUSTED_CLIENT = "merchant-trusted-client";
    public static final String CONSUMER_TRUSTED_CLIENT = "consumer-trusted-client";
    public static final String TRUSTED_CLIENT = "trusted-client";

    public static class ClientRoleConstants {
        public static final String ROLE_USER = "ROLE_USER";
    }

//    // base entity property
//    public static final String[] BASE_PROPERTIES = new String[]{"id", "version", "modificationDate", "creationDate"};
//
//    // ignore properties on update gojek
//    public static final String[] UPDATE_EXPENSE_IGNORE_PROP = new String[]{"id", "version", "modificationDate",
//            "creationDate", "photoUrl", "photoFileName"};
//
//    public static final String AT_SIGN = "@";
//
//    public static final String AREA_RADIUS = "AREA_RADIUS";
//    public static final String AREA_RADIUS_2ND = "AREA_RADIUS_2ND";
//
//    public static final BigDecimal PERCENTAGE_DRIVER_CUT = new BigDecimal(75);
//    public static final BigDecimal PERCENTAGE_DIVIDE = new BigDecimal(100);
//    public static final BigDecimal DEFAULT_VALUE_SHOPPING_ADD_PRICE = new BigDecimal(10000);
//
//    public static final Integer DEFAULT_GOJEK_POI_LIMIT = 4;
//    public static final Integer DEFAULT_GOOGLE_POI_LIMIT = 6;
//
//    public static final DecimalFormat ONE_COMA_FORMAT = new DecimalFormat("#.#");
//    public static final DecimalFormat TWO_COMA_FORMAT = new DecimalFormat("#.##");
//
//    public static final DecimalFormat MONEY_FORMAT_WITHOUT_COMMA = new DecimalFormat("###,###");
//
//    public static final SimpleDateFormat STANDART_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
//
//    public static final Integer ETA_DEFAULT = new Integer(600); //seconds
//
//    public static class ClientRoleConstants {
//        public static final String ROLE_USER = "ROLE_USER";
//    }
//
//    public static class RoleConstants {
//        public static final Integer ROLE_ADMIN = 0;
//        public static final Integer ROLE_SUPERVISOR = 1;
//        public static final Integer ROLE_USER = 2;
//    }
//
//    public static class Status {
//        public static final Integer NON_ACTIVE = 0;
//        public static final Integer ACTIVE = 1;
//    }
//
//    public static class StatusExist {
//        public static final Integer DELETED = 0;
//        public static final Integer EXIST = 1;
//    }
//
//    public static class Driver {
//        public static class Status {
//            public static final Integer NON_ACTIVE = 0;
//            public static final Integer ACTIVE = 1;
//            public static final Integer ON_JOB = 2;
//            public static final Integer RESIGN = 3;
//            public static final Integer FIRED = 4;
//            public static final Integer SUSPENDED = 5;
//        }
//
//        public static class Tipe {
//            public static final Integer NEW_REGULAR = 0;
//            public static final Integer REGULAR = 1;
//            public static final Integer DEDICATED = 2;
//            public static final Integer SUSPENDED = 3;
//            public static final Integer RESIGN = 4;
//
//            public static final String NEW_REGULAR_VALUE = "Active New Regular Driver";
//            public static final String REGULAR_VALUE = "Active Regular Driver";
//            public static final String DEDICATED_VALUE = "Active Dedicated Driver";
//            public static final String SUSPENDED_VALUE = "Suspended";
//            public static final String RESIGN_VALUE = "Resign";
//
//        }
//
//        public static class Search {
//        	/**
//        	 * RADIUS in kilometer
//        	 */
//            public static final double RADIUS = 1;
//
//        }
//
//        public static class PulsaLogTypeName {
//        	/**
//        	 * pencairan deposit yg dilakukan oleh finance
//        	 */
//        	public static final String WITHDRAW = "WITHDRAW";
//        	/**
//        	 * pengurangan karena order
//        	 */
//        	public static final String COMMISION = "COMMISION";
//        	/**
//        	 * penambahan karena driver melakukan deposit dari uang dia sendiri
//        	 */
//        	public static final String TOPUP_CASH = "TOPUP CASH";
//        	/**
//        	 * penambahan non-cash karena order corporate
//        	 */
//        	public static final String TOPUP_CORP = "TOPUP CORP";
//        	/**
//        	 * penambahan karena customer yg menggunakan voucher
//        	 */
//        	public static final String TOPUP_VOUCHER = "TOPUP VOUCHER";
//        	/**
//        	 * initial value saat penambahan driver
//        	 */
//        	public static final String INITIAL = "INITIAL";
//        }
//
//    }
//
//    public static class Route {
//        public static class Status {
//            public static final Integer UNSET = 0;
//            public static final Integer CUSTOMER_PICKED_UP = 1;
//            public static final Integer ON_PROCESS = 2;
//            public static final Integer DONE = 3;
//        }
//    }
//
//    public static class Booking {
//
//        public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy'T'HH:mm");
//
//        public static class Status {
//            public static final Integer UNSET = 0;
//            public static final Integer BID = 1;
//            public static final Integer CONFIRM_BID = 2;
//            public static final Integer ON_PROCESS = 3;
//            public static final Integer DONE = 4;
//            public static final Integer NO_DRIVER = 5;
//            public static final Integer SEARCHING_DRIVER = 6;
//            public static final Integer PICKUP_DONE = 7;
//        }
//
//        public static class Flag {
//            public static final Integer CANCELED = 0;
//            public static final Integer DISCONNECTED = 1;
//            public static final Integer SUBMITTED = 2;
//            public static final Integer CLOSED = 3;
//            public static final Integer CHECK_PRICE = 4;
//        }
//
//        public static class Type {
//        	public static final Integer TRANSPORTATION = 1;
//            public static final Integer COURIER = 2;
//            public static final Integer SHOPPING = 3;
//            public static final Integer MARKETPLACE = 4;
//            public static final Integer GO_FOOD = 5;
//        }
//
//        public static class TimeType {
//        	public static final Integer ORDER = 0;
//            public static final Integer PRE_ORDER = 1;
//        }
//
//        public static class NeedAttentionStage{
//        	public static final Integer NO_DRIVER = 0;
//        	public static final Integer CUST_PICKED_UP = 1;
//        	public static final Integer BOOKING_CLOSED = 2;
//        }
//    }
//
//    public static class Voucher {
//    	public static class Type {
//    		public static final Integer AMOUNT = 0;
//    		public static final Integer FREE_RIDE = 1;
//    	}
//    }
//
//    public static class Parameter {
//        public static String MINIMUM_FARE = "MINIMUM FARE";
//        public static String FARE_PER_KM = "FARE PER KM";
//        public static String BID_TIME = "BID_DURATION_LIMIT";
//        public static String DRIVER_CUT = "DRIVER_CUT";
//
//        public static String BID_MAX_RIPPLE = "BID_MAX_RIPPLE";
//
//        public static String GOJEK_POI_LIMIT = "GOJEK_POI_LIMIT";
//        public static String GOOGLE_POI_LIMIT = "GOOGLE_POI_LIMIT";
//
//        public static String GEOCODE_RADIUS = "GEOCODE_RADIUS";
//
//        public static String PLACES_LOCATION = "PLACES_LOCATION";
//        public static String PLACES_RADIUS = "PLACES_RADIUS";
//        public static String PLACES_KEY = "PLACES_KEY";
//
//        public static String SHOPPING_ADD_PRICE = "SHOPPING_ADD_PRICE";
//        public static String MARKETPLACE_PRICE = "MARKETPLACE_PRICE";
//
//        public static final String DEV_STATUS = "DEV_STATUS";
//        public static final String GOJEK_KEY_STRING = "GOJEK_KEY_STRING";
//        public static final String GOJEK_CLIENT_ID = "GOJEK_CLIENT_ID";
//
//        public static final String DRIVERBOOKING_SMS_SWITCH = "DRIVERBOOKING_SMS_SWITCH";
//
//        public static final String NO_DRIVER_AUTO_CANCEL = "NO_DRIVER_AUTO_CANCEL";
//
//        public static final String SWITCH_IS_STRESSTEST = "SWITCH_IS_STRESSTEST";
//
//        public static final String BID_MAX_BLAST = "BID_MAX_BLAST";
//    }
//
//    public static class Gcm {
//        public static String MESSAGE = "message";
//    }
//
//    public static final class GCMNotificationType {
//        public static final String NEW_JOB = "NEW_JOB";
//        public static final String NEW_JOB_BIDDING = "NEW_JOB_BIDDING";
//        public static final String SUCCESS_BIDDING = "SUCCESS_BIDDING";
//        public static final String FAILED_BIDDING = "FAILED_BIDDING";
//        public static final String ALERT = "ALERT";
//        public static final String ERROR = "ERROR";
//        public static final String SELF_KILL = "SELF_KILL";
//        public static final String CANCEL_BOOKING = "CANCEL_BOOKING";
//        public static final String EDIT_BOOKING = "EDIT_BOOKING";
//        public static final String CLOSE_BOOKING = "CLOSE_BOOKING";
//        public static final String UPDATE_DEPOSIT = "UPDATE_DEPOSIT";
//    }
//
//    public static class GCMCustomerNotificationType{
//        public static final String NO_DRIVER = "NO_DRIVER";
//        public static final String DRIVER_FOUND = "DRIVER_FOUND";
//        public static final String PICKUP_DONE = "PICKUP_DONE";
//        public static final String BOOKING_COMPLETE = "BOOKING_COMPLETE";
//    }
//
//    public static final class Location {
//        public static final String LOCATION_KEY = "UserLocation";
//    }
//
//    public static final class HttpHeaderInfo {
//        public static final String TOTAL_PAGE_HEADER = "Total-Pages";
//        public static final String TOTAL_COUNT_HEADER = "Total-Count";
//        public static final String ERROR_MESSAGE = "Error-Message";
//
//    }
//
//    public static final class PageParameter{
//        public static final String LIST_DATA = "listData";
//        public static final String TOTAL_PAGES = "totalPages";
//        public static final String TOTAL_ELEMENTS = "totalElements";
//    }
//
//    public static final class Bid {
//        public static class Status {
//            public static final Integer DELIVERED = 0;
//            public static final Integer BID = 1;
//            public static final Integer REJECT = 2;
//            public static final Integer CLOSE = 3;
//            public static final Integer WIN = 4;
//        }
//
//        // Default bid time (miliseconds)
//        public static final Long DEFAULT_BID_TIME = (long)30000;
//    }
//
//    public static final class Audit {
//    	public static class Module {
//    		public static final Integer USER = 1;
//        	public static final Integer ROLE = 2;
//        	public static final Integer BOOKING = 3;
//        	public static final Integer CUSTOMER = 4;
//        	public static final Integer DRIVER = 5;
//        	public static final Integer PANGKALAN = 6;
//        	public static final Integer PARAMETER = 7;
//        	public static final Integer POI = 8;
//        	public static final Integer VOUCHER = 9;
//    	}
//
//    	public static class Activity {
//    		public static final Integer ADD = 1;
//    		public static final Integer UPDATE = 2;
//    		public static final Integer DELETE = 3;
//    	}
//    }
//
//    public static final class Corporate {
//        public static final class PriceOption {
//            public static final Integer NO_CHANGE = 0;
//            public static final Integer VARIABLE = 1;
//            public static final Integer FIXED = 2;
//        }
//
//        public static final class InsuranceOption {
//            public static final Integer OFF = 0;
//            public static final Integer ON = 1;
//        }
//    }
//
//    public static final class Marketplace {
//
//        public static final class Type {
//            public static final Integer DELIVERY_ONLY = 0;
//            public static final Integer CASH_ON_DELIVERY = 1;
//            public static final Integer CASH_ON_DELIVERY_RETURN = 2;
//            public static final Integer TRANSFER_ON_DELIVERY = 3;
//        }
//
//        public static final class Region {
//            public static final Integer JAKARTA = 0;
//            public static final Integer OUTSIDE_JAKARTA = 1;
//        }
//
//        public static final class TodPaymentType {
//            public static final Integer ATM = 0;
//            public static final Integer M_BANKING = 1;
//            public static final Integer INTERNET_BANKING = 2;
//        }
//
//        public static final class Parameter {
//            public static final String DO_JKT_SELLER = "DO_JKT_SELLER";
//            public static final String DO_JKT_BUYER = "DO_JKT_BUYER";
//            public static final String DO_OUT_JKT_SELLER = "DO_OUT_JKT_SELLER";
//            public static final String DO_OUT_JKT_BUYER = "DO_OUT_JKT_BUYER";
//
//            public static final String COD_JKT_SELLER = "COD_JKT_SELLER";
//            public static final String COD_JKT_BUYER = "COD_JKT_BUYER";
//            public static final String COD_OUT_JKT_SELLER = "COD_OUT_JKT_SELLER";
//            public static final String COD_OUT_JKT_BUYER = "COD_OUT_JKT_BUYER";
//
//            public static final String CODR_JKT_SELLER = "CODR_JKT_SELLER";
//            public static final String CODR_JKT_BUYER = "CODR_JKT_BUYER";
//            public static final String CODR_OUT_JKT_SELLER = "CODR_OUT_JKT_SELLER";
//            public static final String CODR_OUT_JKT_BUYER = "CODR_OUT_JKT_BUYER";
//
//            public static final String TOD_JKT_SELLER = "TOD_JKT_SELLER";
//            public static final String TOD_JKT_BUYER = "TOD_JKT_BUYER";
//            public static final String TOD_OUT_JKT_SELLER = "TOD_OUT_JKT_SELLER";
//            public static final String TOD_OUT_JKT_BUYER = "TOD_OUT_JKT_BUYER";
//
//        }
//    }
//
//    public static final class Position {
//
//        public static final String SUPERVISOR = "SUPERVISOR";
//
//    }
//
//    public static final class FileGenerator {
//    	public static final String DRIVER_DEPOSIT = "DRIVERDEPOSIT";
//    	public static final String CUST_CREDIT = "CUSTCREDIT";
//    }
//
//    public static final class UpdateRouteResponseCode {
//        public static final Integer BOOKING_NOT_FOUND = 0;
//        public static final Integer BOOKING_CANCELED = 1;
//        public static final Integer BOOKING_CLOSED = 2;
//        public static final Integer DRIVER_NOT_SAME = 3;
//        public static final Integer DRIVER_DEPOSIT_INSUFFICIENT = 4;
//    }
//
//    public static final class BidKey{
//        public static final String BOOKING_ID = "BOOKING_ID";
//        public static final String TRIGGER_KEY = "TRIGGER_KEY";
//        public static final String CURRENT_RIPPLE = "CURRENT_RIPPLE";
//        public static final String RIPPLE_COUNT = "RIPPLE_COUNT";
//        public static final String PROCESS_COUNT = "PROCESS_COUNT";
//        public static final String CURRENT_PROCESS = "CURRENT_PROCESS";
//        public static final String BID_ID = "BID_ID";
//
//        public static final String DEFAULT_BID_TASK_GROUP = "bidTaskGroup";
//    }
//
//    public static final class Merchant {
//        public static class Status {
//            public static final Integer NOT_OPEN = 0;
//            public static final Integer PREPARING = 1;
//            public static final Integer READY_TO_PICKUP = 2;
//            public static final Integer PICKUP_DONE = 3;
//            public static final Integer DELIVERED = 4;
//        }
//    }

}
