package com.nostratech.gojek.util;

import java.util.Arrays;
import java.util.List;

/**
 * Common String helper utilities
 *
 * Created by andri.khrisharyadi@nostratech.com
 * on 3/20/14.
 */
public class StringUtils {

    /**
     * Extract long string value to be List of String
     * ex : [ aaa, bbb ]
     * @return List<String>
     */
    public static List<String> extractStringToList(String value, String splitter){

        String[] arrays = value.split(splitter);

        return Arrays.asList(arrays);

    }

}
