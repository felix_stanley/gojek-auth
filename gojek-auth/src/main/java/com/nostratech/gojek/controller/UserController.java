package com.nostratech.gojek.controller;

import com.nostratech.gojek.service.authentication.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

/**
 * User Controller
 * Created by andri.khrisharyadi@nostratech.com
 * on 3/19/14.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    @Qualifier(value = "userAuthenticationServiceImpl")
    AuthenticationService authenticationService;

    @RequestMapping(value = "/encode-password/{plain}", method = RequestMethod.GET)
    public String generateEncodedPassword(@PathVariable("plain") String plain){
        return authenticationService.encodePassword(plain);
    }

}
