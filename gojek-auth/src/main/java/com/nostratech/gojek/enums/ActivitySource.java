package com.nostratech.gojek.enums;

/**
 * 
 * @author bergasbimo
 *	actor: 0 unset, 1=customer_ios_app, 2=customer_android_app, 3=dispatch_app, 4=driver_app, 5=finance_app, 6=system 
 */
public enum ActivitySource implements BaseModelEnum<Integer> {
	UNSET(0),
    CUSTOMER_IOS_APP(1),
    CUSTOMER_ANDROID_APP(2),
    DISPATCH_APP(3),
    DRIVER_APP(4),
    FINANCE_APP(5),
    SYSTEM(6);

    private Integer internalValue;

    private ActivitySource(Integer internalValue){
        this.internalValue = internalValue;
    }

    @Override
    public Integer getInternalValue() {
        return this.internalValue;
    }

    /**
     * Get ActivitySource enum from ordinal
     *
     * @param ordinal
     * @return
     */
    public static ActivitySource getDataFromOrdinal(int ordinal){
        return ActivitySource.values()[ordinal];
    }

}
