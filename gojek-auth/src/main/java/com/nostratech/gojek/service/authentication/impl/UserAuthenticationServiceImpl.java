package com.nostratech.gojek.service.authentication.impl;

import com.nostratech.gojek.model.domain.User;
import com.nostratech.gojek.persistence.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by aguswinarno on 1/21/15.
 */
@Service
public class UserAuthenticationServiceImpl extends BaseAuthenticationServiceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthenticationServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public String doAuthentication(String username, String rawPassword) {
        LOGGER.info("[LOGIN] login {} started",username);

        User user = userRepository.findByUsername(username);

        if(user != null && matches(rawPassword, user.getPassword())) {
            return user.getUsername();
        }

        return null;
    }

}
