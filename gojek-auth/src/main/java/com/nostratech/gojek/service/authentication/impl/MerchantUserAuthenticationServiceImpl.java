package com.nostratech.gojek.service.authentication.impl;

import com.nostratech.gojek.exception.AuthenticationException;
import com.nostratech.gojek.model.domain.MerchantUser;
import com.nostratech.gojek.persistence.repository.MerchantUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by aguswinarno on 3/12/15.
 */

@Service
public class MerchantUserAuthenticationServiceImpl extends BaseAuthenticationServiceImpl {

    @Autowired
    MerchantUserRepository merchantUserRepository;

    @Transactional(readOnly = true)
    public String doAuthentication(String username, String password){
        List<MerchantUser> merchantUsers = merchantUserRepository.findByEmailOrderByLastLoginDesc(username);

        if( null == merchantUsers || merchantUsers.size() == 0 ) throw new AuthenticationException("Username not found", HttpStatus.UNAUTHORIZED);

        MerchantUser merchantUser = merchantUsers.get(0);

        if( !matches(password, merchantUser.getPassword()) ) throw new AuthenticationException("Username and password not match", HttpStatus.UNAUTHORIZED);

        return merchantUser.getEmail();
    }

}
