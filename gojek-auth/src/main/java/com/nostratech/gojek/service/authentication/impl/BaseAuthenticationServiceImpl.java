package com.nostratech.gojek.service.authentication.impl;

import com.nostratech.gojek.service.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

/**
 * Created by aguswinarno on 9/25/14.
 */
public abstract class BaseAuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    StandardPasswordEncoder standardPasswordEncoder;

    public String encodePassword(String rawPassword) {
        return standardPasswordEncoder.encode(rawPassword);
    }

    public boolean matches(String rawPassword, String encodedPassword)  {
        return standardPasswordEncoder.matches(rawPassword, encodedPassword);
    }

}
