package com.nostratech.gojek.service.authentication;

/**
 * Created by aguswinarno on 9/24/14.
 */
public interface AuthenticationService {

    public String encodePassword(String rawPassword);

    public boolean matches(String rawPassword, String encodedPassword);

    public String doAuthentication(String username, String rawPassword);

}
