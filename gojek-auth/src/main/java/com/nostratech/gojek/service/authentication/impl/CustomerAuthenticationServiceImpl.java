package com.nostratech.gojek.service.authentication.impl;

import com.nostratech.gojek.exception.AuthenticationException;
import com.nostratech.gojek.model.domain.Customer;
import com.nostratech.gojek.persistence.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by aguswinarno on 9/24/14.
 */

@Service
public class CustomerAuthenticationServiceImpl extends BaseAuthenticationServiceImpl {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public String doAuthentication(String username, String rawPassword) {

        String email = username;

        //Customer customer = customerRepository.findByEmail(email);

        List<Customer> customerList = customerRepository.findByEmailOrderByLastLoginDesc(email);

        if( customerList.size() == 0 ) throw new AuthenticationException("Username not found", HttpStatus.UNAUTHORIZED);

        Customer customer = customerList.get(0);

        if( !matches(rawPassword, customer.getPassword()) ) throw new AuthenticationException("Username and password not match", HttpStatus.UNAUTHORIZED);

        updateCustomerLoginData(customer);

        return customer.getEmail();
    }

    private void updateCustomerLoginData(Customer customer) {

        if(null != customer) {
            customer.setLastLogin(new Date());
            customerRepository.saveAndFlush(customer);
        }

    }
}
