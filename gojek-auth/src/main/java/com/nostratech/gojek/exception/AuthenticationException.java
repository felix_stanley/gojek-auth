package com.nostratech.gojek.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by aguswinarno on 9/24/14.
 */
public class AuthenticationException extends RuntimeException {

    private HttpStatus httpStatus;

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String s) {
        super(s);
    }

    public AuthenticationException(String s, HttpStatus httpStatus) {
        super(s);
        this.httpStatus = httpStatus;
    }

    public AuthenticationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AuthenticationException(Throwable throwable) {
        super(throwable);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
