package com.nostratech.gojek.exception;

import org.springframework.http.HttpStatus;

/**
 * Digunakan untuk handling error message dengan exception
 * untuk pengecekan data secara manual bukan melalui fitur bawaan jpa
 *
 * Created by aguswinarno on 8/6/14.
 */
public class ServiceVOException extends RuntimeException {

    private HttpStatus httpStatus;

    public ServiceVOException() {
    }

    public ServiceVOException(String s) {
        super(s);
    }

    public ServiceVOException(String s, HttpStatus httpStatus) {
        super(s);
        this.httpStatus = httpStatus;
    }

    public ServiceVOException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ServiceVOException(Throwable throwable) {
        super(throwable);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
