package com.nostratech.gojek.converter;

import com.nostratech.gojek.model.domain.Base;

import java.util.List;

/**
 * Created by aguswinarno on 7/13/14.
 */
public interface BaseVoConverter<V, T extends Base> {

    /**
     * transfer value from vo object to domain object
     * for enum value, please do manually using Enum.values()[ordinal]
     * @param vo
     * @param model
     * @return
     */
    public T transferVOToModel(V vo, T model);

    /**
     * transfer value from list of domain object to list of vo object
     * @param models
     * @param vos
     * @return
     */
    public List<V> transferListOfModelToListOfVO(List<T> models, List<V> vos);

    /**
     * transfer value from domain object to vo object
     * @param model
     * @param vo
     * @return
     */
    public V transferModelToVO(T model, V vo);
}
