package com.nostratech.gojek.converter;

import com.nostratech.gojek.model.domain.User;
import com.nostratech.gojek.model.vo.UserVO;
import com.nostratech.gojek.util.ExtendedSpringBeanUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aguswinarno on 7/14/14.
 */
@Component
public class UserVoConverter implements BaseVoConverter<UserVO, User> {
    @Override
    public User transferVOToModel(UserVO vo, User model) {

        if(null == model) model = new User();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"username", "firstname", "lastname", "password", "keterangan", "email", "enabled", "roleId"},
                new String[]{"username", "firstname", "lastname", "password", "keterangan", "email", "enabled", "roleId"});

        return model;
    }

    @Override
    public List<UserVO> transferListOfModelToListOfVO(List<User> models, List<UserVO> vos) {

        if(null == vos) vos = new ArrayList<UserVO>();

        if(null == models) return vos;

        for (User user : models) {
            UserVO userVO = new UserVO();
            transferModelToVO(user, userVO);
            vos.add(userVO);
        }

        return vos;
    }

    @Override
    public UserVO transferModelToVO(User model, UserVO vo) {

        if(null == vo) vo = new UserVO();

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"id", "username", "firstname", "lastname", "password", "keterangan", "email", "enabled", "roleId"},
                new String[]{"id", "username", "firstname", "lastname", "password", "keterangan", "email", "enabled", "roleId"});

        return vo;
    }
}
