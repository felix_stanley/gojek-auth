package com.nostratech.gojek.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Base entity class
 */
@MappedSuperclass
public abstract class Base implements Serializable {

	private static final long serialVersionUID = -7369920601847524273L;

	public Base() {

	}

//	@TableGenerator(name = "seq_generator", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "base_seq")
	@Id
//	@GeneratedValue(strategy = GenerationType.TABLE, generator = "seq_generator")
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_last_modified")
	private Date modificationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date creationDate;

	@Version
	@Column(name = "version")
	private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @PrePersist
    public void prePersist() {
        this.modificationDate = new Date();
        this.creationDate = new Date();
    }


    @PreUpdate
    public void preUpdate() {
        this.modificationDate = new Date();
    }

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
//		if (this.equals(obj))
//			return true;
		if (getClass() != obj.getClass())
			return false;
		Base other = (Base) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
