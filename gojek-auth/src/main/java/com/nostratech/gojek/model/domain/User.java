package com.nostratech.gojek.model.domain;


import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;


/**
 * entity class : table user
 *
 * @author andri.khrisharyadi@nostratech.com
 *
 * Mar 18, 2014 : 11:26:29 AM	
 */
@Entity
@NamedQueries({
       @NamedQuery(name = "User.findByUsernameAndPassword", query = "select object(us) from User us where us.username = :USERNAME  and us.password = :PASSWORD")
})
@Table(name = "user", indexes = {
        @Index(columnList = "username", name = "user_username_hidx")
})
@DynamicUpdate
public class User extends Base {

	@Column(name = "username", length = 100, nullable = false, unique = true)
	private String username;

    @Column(name = "first_name", length = 100)
    private String firstname;

    @Column(name = "last_name", length = 100)
    private String lastname;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "keterangan")
	private String keterangan;

    @Column(name = "email")
    private String email;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "role_id")
    private Integer roleId;

    public User() { }

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String description) {
		this.keterangan = description;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @PrePersist
    public void prePersist() {
        super.prePersist();
        if(this.enabled == null) {
            this.enabled = Boolean.TRUE;
        }
    }
	
}
