package com.nostratech.gojek.model.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by aguswinarno on 10/21/14.
 */
@Entity
@Table(name = "customer" , indexes = {
        @Index(columnList = "name", name = "customer_name_hidx"),
        @Index(columnList = "phone", name = "customer_phone_hidx")
})
public class Customer extends Base {

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "phone", length = 30, nullable = false, unique = true)
    private String phone;

    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "corporate_id")
    private Integer corporateId;

    @Column(name = "credit_balance")
    private BigDecimal creditBalance;

    @Column(name = "password")
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
    private Date lastLogin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_logout")
    private Date lastLogout;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "signup_date")
    private Date signupDate;
    
    @Column(name = "referral_credit_balance")
    private BigDecimal referralCreditBalance;
    
    @Column(name = "is_new_customer")
    private Boolean newCustomer;
    
    @Column(name = "is_blacklisted")
    private Boolean blacklisted;

    public Customer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(Integer corporateId) {
        this.corporateId = corporateId;
    }

    public BigDecimal getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(BigDecimal voucherBalance) {
        this.creditBalance = voucherBalance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getLastLogout() {
        return lastLogout;
    }

    public void setLastLogout(Date lastLogout) {
        this.lastLogout = lastLogout;
    }

    public Date getSignupDate() {
		return signupDate;
	}

	public void setSignupDate(Date signupDate) {
		this.signupDate = signupDate;
	}

	public BigDecimal getReferralCreditBalance() {
		return referralCreditBalance;
	}

	public void setReferralCreditBalance(BigDecimal referralBonusBalance) {
		this.referralCreditBalance = referralBonusBalance;
	}

	public Boolean getNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(Boolean newCustomer) {
		this.newCustomer = newCustomer;
	}

	public Boolean getBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(Boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	@Override
    public void prePersist() {
        super.prePersist();
        if (null == this.creditBalance) this.creditBalance = BigDecimal.ZERO;
        if (null == this.blacklisted) this.blacklisted = Boolean.FALSE;
        this.newCustomer = Boolean.TRUE;
    }
}
