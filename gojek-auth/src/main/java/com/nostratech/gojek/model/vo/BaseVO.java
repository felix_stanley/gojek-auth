package com.nostratech.gojek.model.vo;

import java.io.Serializable;

/**
 * Base View Object
 * Created by andri.khrisharyadi@gmail.com
 * on 5/7/14.
 */
public abstract class BaseVO implements Serializable {

    protected Integer id;

    public BaseVO(){
    }

    public BaseVO(Integer id){
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
