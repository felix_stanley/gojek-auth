package com.nostratech.gojek.persistence.repository;

import com.nostratech.gojek.model.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aguswinarno on 7/2/14.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    public List<Customer> findByEmailOrderByLastLoginDesc(String email);
    
}
