package com.nostratech.gojek.persistence.repository;

import com.nostratech.gojek.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User Repository
 * @author andri.khrisharyadi@nostratech.com
 * Mar 18, 2014 : 1:09:52 PM	
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>  {
	
	public User findByUsername(String username);

}
