package com.nostratech.gojek.persistence.repository;

import com.nostratech.gojek.model.domain.MerchantUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aguswinarno on 3/12/15.
 */
@Repository
public interface MerchantUserRepository extends JpaRepository<MerchantUser, Integer> {

    public List<MerchantUser> findByEmailOrderByLastLoginDesc(String email);

}
