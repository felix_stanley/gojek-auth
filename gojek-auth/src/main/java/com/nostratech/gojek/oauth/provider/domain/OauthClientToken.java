package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.*;

/**
 * domain object for oauth_client_token from oauth2 spring security
 * 
 * @author aguswinarno
 * 
 */

@Entity
@Table(name = "oauth_client_token")
public class OauthClientToken {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "TOKEN_ID", length = 256)
	private String tokenId;

    @Lob
    @Column(name = "TOKEN")
    private byte[] token;

	@Column(name = "AUTHENTICATION_ID", length = 256)
	private String authenticationId;

	@Column(name = "USER_NAME", length = 256)
	private String userName;

	@OneToOne(cascade = CascadeType.ALL)
	private OauthClientDetails clientId;

	public OauthClientToken() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public byte[] getToken() {
		return token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public OauthClientDetails getClientId() {
		return clientId;
	}

	public void setClientId(OauthClientDetails clientId) {
		this.clientId = clientId;
	}

}
