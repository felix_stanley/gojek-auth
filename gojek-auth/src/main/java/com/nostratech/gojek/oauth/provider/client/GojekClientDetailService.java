package com.nostratech.gojek.oauth.provider.client;

import com.nostratech.gojek.oauth.provider.dao.OauthClientDetailsDAO;
import com.nostratech.gojek.oauth.provider.domain.OauthClientDetails;
import com.nostratech.gojek.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.List;


public class GojekClientDetailService implements ClientDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(GojekClientDetailService.class);

    private OauthClientDetailsDAO clientDetailsDAO;

    public OauthClientDetailsDAO getClientDetailsDAO() {
        return clientDetailsDAO;
    }

    public void setClientDetailsDAO(OauthClientDetailsDAO clientDetailsDAO) {
        this.clientDetailsDAO = clientDetailsDAO;
    }

    public GojekClientDetailService() {
    }

    public ClientDetails loadClientByClientId(String clientId)
            throws OAuth2Exception {

        logger.info("FINDING Client_Id "+clientId);

        BaseClientDetails details = null;
        try {

            OauthClientDetails clientDetails = clientDetailsDAO.findById(clientId);

            if (clientDetails != null) {
                logger.info("FOUND Client_Id "+clientId);
                details = new BaseClientDetails(clientDetails.getResourceIds(), clientDetails.getScope(), clientDetails.getAuthorizedGrantTypes(), clientDetails.getAuthorities(),
                        clientDetails.getWebServerRedirectUri());

                List<String> authorizedGrantTypes = StringUtils.extractStringToList(clientDetails.getAuthorizedGrantTypes(), ",");
                details.setAuthorizedGrantTypes(authorizedGrantTypes);

                List<String> scopes = StringUtils.extractStringToList(clientDetails.getScope(), ",");
                details.setScope(scopes);

                details.setClientId(clientDetails.getClientId());
                details.setClientSecret(clientDetails.getClientSecret());
                details.setAccessTokenValiditySeconds(clientDetails.getAccessTokenValidity() == null ? 0 : clientDetails.getAccessTokenValidity());
                details.setRefreshTokenValiditySeconds(clientDetails.getRefreshTokenValidity() == null ? 0 : clientDetails.getRefreshTokenValidity());

                details.setResourceIds(clientDetails.getResourceIds() == null ? null : StringUtils.extractStringToList(clientDetails.getResourceIds(), ","));

            }


        } catch (EmptyResultDataAccessException e) {
            logger.error("NOT FOUND Client_Id "+clientId);
            throw new InvalidClientException("Client not found: " + clientId);
        }

        return details;

    }

}
