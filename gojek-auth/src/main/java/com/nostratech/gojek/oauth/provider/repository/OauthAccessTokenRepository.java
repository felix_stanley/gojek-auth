package com.nostratech.gojek.oauth.provider.repository;

import com.nostratech.gojek.oauth.provider.domain.OauthAccessToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * OauthAccessToken Repository
 * Created by andri.khrisharyadi@gmail.com
 * on 3/20/14.
 */
@Repository
public interface OauthAccessTokenRepository extends JpaRepository<OauthAccessToken, Integer> {

}
