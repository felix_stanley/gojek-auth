package com.nostratech.gojek.oauth.provider.client;
	
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Service;

@Service
public class UserGojekClientDetailsService implements UserDetailsService {
	
	private GojekClientDetailService gojekClientDetailService;

	public UserGojekClientDetailsService(GojekClientDetailService gojekClientDetailService) {
		this.gojekClientDetailService = gojekClientDetailService;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		ClientDetails clientDetails = gojekClientDetailService.loadClientByClientId(username);
		String clientSecret = clientDetails.getClientSecret() == null ? "" : clientDetails.getClientSecret();
		return new User(username, clientSecret, clientDetails.getAuthorities());	}

}
