package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.*;

/**
 * domain object for oauth_access_token from oauth2 spring security
 * 
 * @author aguswinarno
 * 
 */
@Entity
@Table(name = "oauth_access_token")
public class OauthAccessToken {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "TOKEN_ID", length = 256, nullable = true)
	private String token_id;

	@Column(name = "TOKEN")
    @Lob
	private byte[] token;

	@Column(name = "AUTHENTICATION_ID", length = 256)
	private String authenticationId;

	@Column(name = "USER_NAME", length = 256)
	private String userName;

	@Column(name = "CLIENT_ID", length = 256)
	private String clientId;

	@Column(name = "AUTHENTICATION")
    @Lob
	private byte[] authentication;

	@Column(name = "REFRESH_TOKEN", length = 256)
	private String refreshToken;

	public OauthAccessToken() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken_id() {
		return token_id;
	}

	public void setToken_id(String token_id) {
		this.token_id = token_id;
	}

	public byte[] getToken() {
		return token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public byte[] getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

}
