package com.nostratech.gojek.oauth.provider.dao;

public interface OauthDAO {
	public Object findById(Object pk);

	public void save(Object domainObject);

	public void delete(Object domainObject);

}
