package com.nostratech.gojek.oauth.provider.repository;

import com.nostratech.gojek.oauth.provider.domain.OauthRefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * OauthRefreshToken Repository
 * Created by andri.khrisharyadi@gmail.com
 * on 3/20/14.
 */
public interface OauthRefreshTokenRepository extends JpaRepository<OauthRefreshToken, Integer> {



}
