package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.*;

/**
 * domain object for oauth_refresh_token from oauth2 spring security
 * 
 * @author aguswinarno
 * 
 */
@Entity
@Table(name = "oauth_refresh_token")
public class OauthRefreshToken {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "TOKEN_ID", length = 256)
	private String token_id;

    @Lob
    @Column(name = "TOKEN")
	private byte[] token;

    @Lob
    @Column(name = "AUTHENTICATION")
	private byte[] authentication;

    public OauthRefreshToken() {
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken_id() {
		return token_id;
	}

	public void setToken_id(String token_id) {
		this.token_id = token_id;
	}

	public byte[] getToken() {
		return token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public byte[] getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

}
