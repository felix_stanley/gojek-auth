package com.nostratech.gojek.oauth.provider.dao;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

public abstract class OauthBaseDAO implements OauthDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void delete(Object domainObject) {
        this.entityManager.remove(domainObject);
        this.entityManager.setFlushMode(FlushModeType.AUTO);
    }

    public void save(Object domainObject) {
        this.getEntityManager().merge(domainObject);
        entityManager.flush();
    }

}
