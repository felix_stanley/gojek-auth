package com.nostratech.gojek.oauth.provider.dao;

import com.nostratech.gojek.oauth.provider.domain.OauthAccessToken;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
public class OauthAccessTokenDAO extends OauthBaseDAO {

    public OauthAccessToken findById(Object id) {
        try {
            String queryString = "SELECT object(accessToken) FROM OauthAccessToken accessToken where accessToken.id = :id";
            Query query = super.getEntityManager().createQuery(queryString);
            query.setParameter("id", id);

            return (OauthAccessToken) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
