package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.*;

/**
 * domain object for oauth_code from oauth2 spring security
 * 
 * @author aguswinarno
 * 
 */

@Entity
@Table(name = "oauth_code")
public class OauthCode {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

	@Column(name = "CODE", length = 256)
	private String code;

    @Lob
	@Column(name = "AUTHENTICATION")
    private byte[] authentication;

	public OauthCode() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte[] getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

}
