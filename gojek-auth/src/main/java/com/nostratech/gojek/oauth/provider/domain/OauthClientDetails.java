package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * domain object for oauth_client_details from oauth2 spring security
 *
 * @author aguswinarno
 * 
 */

@Entity
@Table(name = "oauth_client_details")
public class OauthClientDetails {

	@Id
	@Column(name = "CLIENT_ID", length = 256)
	private String clientId;

	@Column(name = "RESOURCE_IDS", length = 256)
	private String resourceIds;

	@Column(name = "CLIENT_SECRET", length = 256)
	private String clientSecret;

	@Column(name = "SCOPE", length = 256)
	private String scope;

	@Column(name = "AUTHORIZED_GRANT_TYPES", length = 256)
	private String authorizedGrantTypes;

	@Column(name = "WEB_SERVER_REDIRECT_URI", length = 256)
	private String webServerRedirectUri;

	@Column(name = "AUTHORITIES", length = 256)
	private String authorities;

	@Column(name = "ACCESS_TOKEN_VALIDITY")
	private Integer accessTokenValidity;

	@Column(name = "REFRESH_TOKEN_VALIDITY")
	private Integer refreshTokenValidity;

	@Column(name = "ADDITIONAL_INFORMATION", length = 4096)
	private String additionalInformation;

    @Column(name = "AUTOAPPROVE", length = 256)
    private String autoApprove;

	public OauthClientDetails() {
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	public String getWebServerRedirectUri() {
		return webServerRedirectUri;
	}

	public void setWebServerRedirectUri(String webServerRedirectUri) {
		this.webServerRedirectUri = webServerRedirectUri;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public Integer getAccessTokenValidity() {
		return accessTokenValidity;
	}

	public void setAccessTokenValidity(Integer accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

	public Integer getRefreshTokenValidity() {
		return refreshTokenValidity;
	}

	public void setRefreshTokenValidity(Integer refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

    public String getAutoApprove() {
        return autoApprove;
    }

    public void setAutoApprove(String autoApprove) {
        this.autoApprove = autoApprove;
    }
}
