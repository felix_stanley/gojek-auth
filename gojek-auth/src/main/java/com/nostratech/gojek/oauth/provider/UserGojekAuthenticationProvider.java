package com.nostratech.gojek.oauth.provider;

import com.nostratech.gojek.service.authentication.impl.CustomerAuthenticationServiceImpl;
import com.nostratech.gojek.service.authentication.impl.MerchantUserAuthenticationServiceImpl;
import com.nostratech.gojek.service.authentication.impl.UserAuthenticationServiceImpl;
import com.nostratech.gojek.util.Constants;
import com.nostratech.gojek.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Custom SpringSecurity authentication provider
 * extended from AuthenticationProvider
 * Created by andri.khrisharyadi@nostratech.com
 * on 3/21/14.
 */
public class UserGojekAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserGojekAuthenticationProvider.class);

    private UserAuthenticationServiceImpl userAuthenticationServiceImpl;

    private CustomerAuthenticationServiceImpl customerAuthenticationServiceImpl;

    private MerchantUserAuthenticationServiceImpl merchantUserAuthenticationService;

    public void setUserAuthenticationServiceImpl(UserAuthenticationServiceImpl userAuthenticationServiceImpl) {
        this.userAuthenticationServiceImpl = userAuthenticationServiceImpl;
    }

    public void setCustomerAuthenticationServiceImpl(CustomerAuthenticationServiceImpl customerAuthenticationServiceImpl) {
        this.customerAuthenticationServiceImpl = customerAuthenticationServiceImpl;
    }

    public void setMerchantUserAuthenticationService(MerchantUserAuthenticationServiceImpl merchantUserAuthenticationService) {
        this.merchantUserAuthenticationService = merchantUserAuthenticationService;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String username = null;

        Map authenticationDetail = (Map) authentication.getDetails();

        if(null != authenticationDetail) {

            String clientId = (String) authenticationDetail.get("client_id");

            if(null != clientId) {

                if( clientId.compareTo(Constants.TRUSTED_CLIENT) == 0) {

                    LOGGER.info("Finding GOJEK USER with username "+authentication.getPrincipal().toString()+"...");

                    username = userAuthenticationServiceImpl.doAuthentication(authentication.getPrincipal().toString(), authentication.getCredentials().toString());

                }
                else if( clientId.compareTo(Constants.CONSUMER_TRUSTED_CLIENT) == 0){

                    LOGGER.info("Finding GOJEK CUSTOMER with username "+authentication.getPrincipal().toString()+"...");

                    try {
                        username = customerAuthenticationServiceImpl.doAuthentication(authentication.getPrincipal().toString(), authentication.getCredentials().toString());
                    }catch (com.nostratech.gojek.exception.AuthenticationException ae){
                        username = null;
                    }

                }else if( clientId.compareTo(Constants.MERCHANT_TRUSTED_CLIENT) == 0){

                    LOGGER.info("Finding GOJEK MERCHANT USER with username "+authentication.getPrincipal().toString()+"...");

                    try {
                        username = merchantUserAuthenticationService.doAuthentication(authentication.getPrincipal().toString(), authentication.getCredentials().toString());
                    }catch (com.nostratech.gojek.exception.AuthenticationException ae){
                        username = null;
                    }

                }


            }
        }

        if(username!=null){
            List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
//            assignUserRole(grantedAuthorities, userFind.getRoles() );

            assignUserRole(grantedAuthorities);

            UsernamePasswordAuthenticationToken auth =
                    new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),
                        authentication.getCredentials(),grantedAuthorities);
            LOGGER.info("[USERNAME] " + username + " is found");
            return auth;
        }else {
            LOGGER.error("[USERNAME] " + authentication.getPrincipal().toString() + " is not found");
            throw new BadCredentialsException("Bad User Credentials.");
        }

    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return true;
    }

    /**
     * Assign UserRole to GrantedAuthority
     * Add UserRole to User
     * Using RoleVO
     * @param authorities GrantedAuthority
     //* @param roles List RoleVO
     */
    private void assignUserRole(List<GrantedAuthority> authorities) {
        authorities.add(new SimpleGrantedAuthority(Constants.ClientRoleConstants.ROLE_USER));
    }

    //private void assignUserRole(List<GrantedAuthority> authorities, List<RoleVO> roles){
    //    for(RoleVO roleVO : roles) {
    //        authorities.add(new SimpleGrantedAuthority(roleVO.getName()));
    //    }
    //}
}
