package com.nostratech.gojek.oauth.provider.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * domain object for oauth_client_details from oauth2 spring security
 *
 * @author aguswinarno
 * 
 */

@Entity
@Table(name = "oauth_approvals")
public class OauthApprovals {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "USERID", length = 256)
	private String userId;

	@Column(name = "CLIENTID", length = 256)
	private String clientId;

	@Column(name = "SCOPE", length = 256)
	private String scope;

	@Column(name = "STATUS", length = 10)
	private String status;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EXPIRESAT")
	private Date expiresAt;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LASTMODIFIEDAT")
	private Date lastModifiedAt;

	public OauthApprovals() {
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Date getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(Date lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }
}
