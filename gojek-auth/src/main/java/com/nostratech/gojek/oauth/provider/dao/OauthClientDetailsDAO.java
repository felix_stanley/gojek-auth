package com.nostratech.gojek.oauth.provider.dao;

import com.nostratech.gojek.oauth.provider.domain.OauthClientDetails;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;


public class OauthClientDetailsDAO extends OauthBaseDAO implements OauthDAO {

	public OauthClientDetails findById(Object clientId) {
		try {
			String queryString = "SELECT c FROM OauthClientDetails c where c.clientId = :clientId";
			Query query = super.getEntityManager().createQuery(queryString);
			query.setParameter("clientId", clientId);

			return (OauthClientDetails) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (NonUniqueResultException nure) {
			throw nure;
		}
	}

}
