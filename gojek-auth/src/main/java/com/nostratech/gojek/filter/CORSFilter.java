package com.nostratech.gojek.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by aguswinarno on 7/17/14.
 */
@Component
public class CORSFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            // CORS "pre-flight" request
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type");
            response.addHeader("Access-Control-Max-Age", "1800");//30 min
            response.addHeader("Access-Control-Expose-Headers", "Total-Count, Total-Pages, Error-Message, "
            		+ "Driver-Count-Active, Driver-Count-Fired, Driver-Count-Non-Active, Driver-Count-On-Job, "
            		+ "Driver-Count-Resign, Driver-Count-Suspended");
        //}
        filterChain.doFilter(request, response);
    }

}
