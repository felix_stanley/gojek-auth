#!/bin/sh

APP_HOME="/Users/fani/Workspaces/Gojek/gojek-auth"
APP_NAME="$APP_HOME/gojek-auth/target/gojek-auth-1.1.8.RELEASE.jar"
LOG_FILE="$APP_HOME/auth.log"

if [ "$1" ]; then
	if [ "$1" = "start" ]; then
		echo "starting $APP_NAME, logfile redirected to $LOG_FILE.."
		rm -f $LOG_FILE
#		nohup java -DROOT_APP=/Users/aguswinarno/bitbucket/gojek-auth -Dlogback.configurationFile=configuration/logback.xml -jar $APP_NAME >> $LOG_FILE &
		java -DROOT_APP=$APP_HOME -Dlogback.configurationFile=configuration/logback.xml -jar $APP_NAME
	else
		if [ "$1" = "stop" ]; then
			echo "stopping $APP_NAME.."
			pkill -f $APP_NAME
			echo "$APP_NAME successfully stopped."
		fi
	fi
else
	echo "please give argument 'start' or 'stop'.."
fi


